resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

data "azurerm_key_vault_secret" "license" {
  name         = "qdc-license-full"
  key_vault_id = var.key_vault_id
}

data "azurerm_key_vault_secret" "cid" {
  name         = "falcon-cid"
  key_vault_id = var.key_vault_id
}

locals {
  virtual_machine_name = "${var.prefix}-${random_id.randomMachineId.hex}"
  admin_username = var.admin_username
  admin_password = random_password.password.result
}

module "qmi-nic" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//qmi-nic"
  
  prefix = local.virtual_machine_name
  location = var.location
  
  resource_group_name = var.resource_group_name
  user_id = var.user_id
}

resource "azurerm_virtual_machine" "vm" {
  name                  = local.virtual_machine_name
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [module.qmi-nic.id]
  vm_size               = var.vm_type

  delete_os_disk_on_termination = true
  
  storage_image_reference {
    id = var.image_reference
  }

  storage_os_disk {
    name              = "${local.virtual_machine_name}-osDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
      computer_name  = local.virtual_machine_name
      admin_username = local.admin_username
      admin_password = local.admin_password
  }

  os_profile_linux_config {
      disable_password_authentication = false
  #    ssh_keys {
  #        path     = "/home/${local.admin_username}/.ssh/authorized_keys"
  #        key_data = "${file("~/.ssh/id_rsa.pub")}"
  #    }
  }

  /*boot_diagnostics {
    enabled     = "true"
    storage_uri = module.qmi-storage-account.primary_blob_endpoint
  }*/

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
  }
  
  provisioner "file" {
    connection {
        type     = "ssh"
        host     = module.qmi-nic.private_ip_address
        user     = local.admin_username
        password = local.admin_password
        timeout  = "60s"
        #private_key = "${file("~/.ssh/id_rsa")}"   
    }
    source      = "${path.module}/scripts"
    destination = "~"
  }

  provisioner "remote-exec" {
    connection {
      type     = "ssh"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      timeout  = "60s"
      #private_key = "${file("~/.ssh/id_rsa")}"
    }

    inline = [
        "echo ${local.admin_password} | sudo -S chmod u+x /home/${local.admin_username}/scripts/*.sh",
        #"sudo /home/${local.admin_username}/scripts/start-services.sh",
        "sudo /home/${local.admin_username}/scripts/set-license.sh '${data.azurerm_key_vault_secret.license.value}'",
        "sudo /home/${local.admin_username}/scripts/falcon.sh '${data.azurerm_key_vault_secret.cid.value}'"
    ]
  }
}

