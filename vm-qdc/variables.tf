### Variables
variable "prefix" {
    default = "QMI-QDC"
}

variable "location" {
    default = "East US"
}

variable "image_reference" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.Compute/images/qdc-base-4.4-2"
}

variable "vm_type" {
  default = "Standard_DS3_v2"
}

variable "resource_group_name" { 
}

variable "admin_username" {
  default = "qmi"
}

variable "user_id" {
}

variable "key_vault_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.KeyVault/vaults/qmisecrets"
}



