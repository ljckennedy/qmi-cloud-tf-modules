
echo "Wait 60 seconds to make sure QDC is running"
sleep 60

licenseBody='{"licKeyString": "'"$1"'"}'

echo "Setting QDC license"
curl -X PUT -H "Content-Type: application/json" -d "$licenseBody" http://localhost:8080/qdc/license/register

echo ""
echo "QDC setup is done!"