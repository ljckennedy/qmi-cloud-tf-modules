#!/bin/bash

sudo systemctl daemon-reload

sudo systemctl enable firewalld
sudo systemctl start firewalld

echo "Restart Postgres"
sudo systemctl enable qdc_pg-11.2.service
sudo systemctl start qdc_pg-11.2.service

echo "Restart QDC"
sudo systemctl enable qdc
sudo systemctl restart qdc

echo "Restart QlikCore"
sudo systemctl enable qlikcore
sudo systemctl restart qlikcore