variable "subnet_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-infra-vnet/providers/Microsoft.Network/virtualNetworks/QMI-Automation-Vnet/subnets/QMI-VM-Deployments"
}

variable "prefix" {
}

variable "resource_group_name" {
}

variable "location" {
  default = "East US"
}

variable "user_id" { 
}


