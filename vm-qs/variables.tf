
variable "prefix" {
  description = "The Prefix used for all resources in this example"
  default = "QMI-QS"
}

variable "location" {
  default = "East US"
}

variable "image_reference" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.Compute/images/qliksense-base-feb20-2"
}

variable "resource_group_name" {
}

variable "vm_type" {
  default = "Standard_DS3_v2"
}

variable "managed_disk_type" {
  default = "Premium_LRS"
}

variable "disk_size_gb" {
  default = "128"
}

variable "admin_username" {
  default = "qmi"
}

variable "user_id" {
}

variable "key_vault_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.KeyVault/vaults/qmisecrets"
}
