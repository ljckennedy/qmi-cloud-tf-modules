resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

resource "random_password" "qlikpassword" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

locals {
  virtual_machine_name = "${var.prefix}-${random_id.randomMachineId.hex}"
  admin_username = var.admin_username
  admin_password = random_password.password.result
}

data "azurerm_key_vault_secret" "serial" {
  name         = "qs-license-pa-serial"
  key_vault_id = var.key_vault_id
}

data "azurerm_key_vault_secret" "control" {
  name         = "qs-license-pa-control"
  key_vault_id = var.key_vault_id
}

data "azurerm_key_vault_secret" "cert-password" {
  name         = "star-qmi-qlikpoc-com-password"
  key_vault_id = var.key_vault_id
}

data "azurerm_key_vault_secret" "sensorsettings" {
  name         = "carbonblack-sensorsettings"
  key_vault_id = var.key_vault_id
}

module "qmi-nic" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//qmi-nic"
  
  prefix = local.virtual_machine_name
  location = var.location
  
  resource_group_name = var.resource_group_name
  user_id = var.user_id
}

resource "azurerm_virtual_machine" "vm" {
  name                  = local.virtual_machine_name
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [module.qmi-nic.id]
  vm_size               = var.vm_type

  delete_os_disk_on_termination = true
  delete_data_disks_on_termination = true

  storage_image_reference {
      id = var.image_reference
  }

  storage_os_disk {
    name              = "${local.virtual_machine_name}-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = var.managed_disk_type
    disk_size_gb      = var.disk_size_gb
  }

  os_profile {
    computer_name  = local.virtual_machine_name
    admin_username = local.admin_username
    admin_password = local.admin_password
  }

  os_profile_windows_config {
    
    provision_vm_agent        = true
    enable_automatic_upgrades = false

    winrm {
      protocol = "http"
    }
  }

  provisioner "file" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
    }
    source      = "${path.module}/scripts"
    destination = "C:/provision"
  }

  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
    }

    inline = [
      "powershell.exe -File C:/provision/resethostname.ps1",
      "powershell.exe -File C:/provision/gen-jwt.ps1",
    ]
  }

  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
    }

    inline = [
      "powershell.exe -File C:/provision/qs-post-cfg.ps1 -Hostname ${var.resource_group_name}.qmi.qlik-poc.com -Serial ${data.azurerm_key_vault_secret.serial.value} -Control ${data.azurerm_key_vault_secret.control.value} -Certpassword ${data.azurerm_key_vault_secret.cert-password.value} -QlikUserPassword ${random_password.qlikpassword.result}",
      "powershell.exe -File C:/provision/carbonblack.ps1 -SensorSettings '${data.azurerm_key_vault_secret.sensorsettings.value}'",
      "powershell.exe -File C:/provision/webconnectors/q-WebConnectors.ps1"
    ]
  }

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
  }
}