output "virtual_machine_id" {
  value = azurerm_virtual_machine.vm.id
}

output "virtual_machine_name" {
  value = "${var.prefix}-${random_id.randomMachineId.hex}"
}

output "admin_username" {
  value = var.admin_username
}

output "admin_password" {
  value = random_password.password.result
}

output "qlik_password" {
  value = random_password.qlikpassword.result
}

output "nic_id" {
  value = module.qmi-nic.id
}

output "nic_private_ip_address" {
  value = module.qmi-nic.private_ip_address
}

output "nic_ip_configuration_name" {
  value = module.qmi-nic.ip_configuration_name
}