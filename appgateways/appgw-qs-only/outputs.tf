output "appgw_hostname" {
  value = local.appgw_hostname
}

output "appgw_id" {
  value = azurerm_public_ip.appgw-ip.id
}

output "appgw_public_ip" {
  value = azurerm_public_ip.appgw-ip.ip_address
}

output "appgw_backend_address_pool_0_id" {
  value = azurerm_application_gateway.qmi-app-gw.backend_address_pool[0].id
}
