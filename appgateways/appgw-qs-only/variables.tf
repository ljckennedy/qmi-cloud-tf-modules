
variable "app_gw_rg" {
  description = "Application Gateway Resource Group"
  default     = "AppGW_RG"
}

variable "location" {
  default = "East US"
}

variable "appgw_hostname" {
  
}

variable "provision_id" {
}

variable "key_vault_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.KeyVault/vaults/qmisecrets"
}

variable "app_gw_subnet" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-infra-vnet/providers/Microsoft.Network/virtualNetworks/QMI-Automation-Vnet/subnets/QMI-App-GW"
}

variable "cert_name" {
  default = "star_qmi_qlik-poc_com"
}

variable "domain" {
  default = "qmi.qlik-poc.com"
}


