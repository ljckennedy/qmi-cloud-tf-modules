<#
Module:             q-user-setup
Author:             Clint Carr
Modified by:        -
Modification History:
 - Deleted disable IPv6
 - Changed the creation of Qlik User to be based on Carbon
 - Added Logging
 - Added comments
last updated:       27/07/2018
Intent: Disable Password complexity, create Qlik user and grant remote desktop rights
#>

Write-Log "Starting q-user-setup.ps1"

Trap {
	Write-Log -Message $_.Exception.Message -Severity "Error"
  	Break
}

### Install Carbon PowerShell Module
Write-Log -Message "Installing carbon"
choco install carbon -y | Out-Null
Import-Module "Carbon"

### create Qlik User
Write-Log -Message "Creating Qlik account"
Install-User -UserName Qlik -Password Qlik1234

### Grant Remote Admin Rights to Qlik User
Write-Log -Message "Granting Qlik account Remote Interactive Logon Right"
Grant-Privilege -Identity $env:COMPUTERNAME\qlik -Privilege SeRemoteInteractiveLogonRight

Write-Log -Message "Adding Qlik user to Remote Desktop Users"
Add-GroupMember -Name 'Remote Desktop Users' -Member $env:COMPUTERNAME\qlik

Write-Log -Message "Adding Qlik user to local Administrators"
Add-GroupMember -Name 'Administrators' -Member $env:COMPUTERNAME\qlik

$regPath = "HKLM:\SYSTEM\CurrentControlSet\Control"
Set-ItemProperty $regPath -Name "ServicesPipeTimeout" -Type DWord -Value 180000
