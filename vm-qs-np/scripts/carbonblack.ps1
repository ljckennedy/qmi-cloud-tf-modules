Param(
    [string] $SensorSettings
)

Write-Log -Message "Installing and configuring Carbon Black"

New-Item C:\provision\sensorsettings.ini | Out-Null
Set-Content C:\provision\sensorsettings.ini $SensorSettings | Out-Null
(Get-Content C:\provision\sensorsettings.ini) -replace ' ',"`r`n" | Set-Content C:\provision\sensorsettings.ini -Force

C:\provision\CarbonBlackClientSetup.exe /S

Write-Log -Message "Carbon Black is configure!"



