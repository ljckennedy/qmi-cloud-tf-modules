
variable "prefix" {
  description = "The Prefix used for all resources in this example"
  default = "QMI-ORACLELINUX"
}

variable "location" {
  default = "East US"
}

variable "resource_group_name" {
}

variable "vm_type" {
  default = "Standard_DS3_v2"
}

variable "admin_username" {
  default = "qmi"
}

variable "user_id" {
}

# variable "subnet_id" {
#   default = "/subscriptions/1f3d4c1d-6509-4c52-8dee-c15fb83f2920/resourceGroups/lkn-rg/providers/Microsoft.Network/virtualNetworks/lkn-vn/subnets/default"
# }
