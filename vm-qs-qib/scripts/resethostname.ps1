# Helper Functions
# ----------------
function New-Credential($u,$p) {
    $secpasswd = ConvertTo-SecureString $p -AsPlainText -Force
    return New-Object System.Management.Automation.PSCredential ($u, $secpasswd)
}
$cred = New-Credential "qservice" "Qlik1234"
#import-module "Carbon"

REG ADD HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System /v ConsentPromptBehaviorAdmin /t REG_DWORD /d 0 /f


Set-Service -Name "QlikLoggingService" -StartupType Automatic
Set-Service -Name "QlikSenseServiceDispatcher" -StartupType Automatic
Set-Service -Name "QlikSenseProxyService" -StartupType Automatic
Set-Service -Name "QlikSenseEngineService"  -StartupType Automatic
Set-Service -Name "QlikSensePrintingService"  -StartupType Automatic
Set-Service -Name "QlikSenseSchedulerService"  -StartupType Automatic
Set-Service -Name "QlikSenseRepositoryService"  -StartupType Automatic
Set-Service -Name "QlikSenseRepositoryDatabase"  -StartupType Automatic


Write-Log "Starting QlikSenseRepositoryDatabase and QlikSenseServiceDispatcher"
Start-Service QlikSenseRepositoryDatabase
Start-Service QlikSenseServiceDispatcher

#Delete certificates
Write-Log "Deleting old certificates"
#Get-ChildItem "$($env:ProgramData)\Qlik\Sense\Repository\Exported Certificates\" | Remove-Item -Recurse
$Certs = Get-ChildItem cert:"CurrentUser\My"
$Certs | %{Remove-Item -path $_.PSPath -recurse -Force}
$Certs = Get-ChildItem cert:"LocalMachine\My"
$Certs | %{Remove-Item -path $_.PSPath -recurse -Force}
$Certs = Get-ChildItem cert:"LocalMachine\Root" | Where-Object { $_.Subject -match 'GEN-QS' }
$Certs | %{Remove-Item -path $_.PSPath -recurse -Force}
$Certs = Get-ChildItem cert:"LocalMachine\Root" | Where-Object { $_.Subject -match 'QMI-QS' }
$Certs | %{Remove-Item -path $_.PSPath -recurse -Force}

Write-Log "Setting new hostname to Host.cfg file"
Write-Log $($env:computername)
$enchostname = [Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes("$($env:computername)"))
Set-Content -Path C:\ProgramData\Qlik\Sense\Host.cfg -Value $enchostname

Write-Log "Recreating Qlik Sense certificates"
# AS if qservice user
Start-Process powershell.exe -ArgumentList "Start-Process cmd.exe -Verb runAs -ArgumentList '/k C:\PROGRA~1\Qlik\Sense\Repository\Repository.exe -bootstrap -standalone -restorehostname'"

#From PS
#Start-Process -FilePath "C:\PROGRA~1\Qlik\Sense\Repository\Repository.exe" -ArgumentList "/bootstrap /standalone /restorehostname" -Verb runAs

#qs
#Start-Process powershell.exe -Credential $cred -ArgumentList "Start-Process cmd.exe -Verb runAs -ArgumentList '/k C:\PROGRA~1\Qlik\Sense\Repository\Repository.exe -bootstrap -standalone -restorehostname'"

$waiting=50
Write-Log "Waiting $waiting secs ..."
Start-Sleep -s $waiting

#- Wait 10 seconds
Write-Log "Restarting Service Dispatcher"
#- Restart Service Dispacher
Restart-Service QlikSenseServiceDispatcher -Force
#- Restart rest of the services


Start-Sleep -s 20

Write-Log "New Certs: CurrentUser\My"
Get-ChildItem cert:"CurrentUser\My"
Write-Log "New Certs: LocalMachine\My"
Get-ChildItem cert:"LocalMachine\My"
Write-Log "New Certs: LocalMachine\Root"
Get-ChildItem cert:"LocalMachine\Root" | Where-Object { $_.Subject -match 'QMI' }
$NewCerts = Get-ChildItem cert:"LocalMachine\Root" | Where-Object { $_.Subject -match 'QMI' }

if ($NewCerts) {
    Write-Log "Restarting all Qlik Sense services"
    Restart-Service QlikSenseServiceDispatcher -Force
    Restart-Service QlikLoggingService -Force
    Restart-Service QlikSenseRepositoryService -Force
    Restart-Service QlikSenseProxyService -Force
    Restart-Service QlikSenseEngineService -Force
    Restart-Service QlikSenseSchedulerService -Force 
    Restart-Service QlikSensePrintingService -Force 

    Start-Sleep -s 20
    Write-Log "Recovering Qlik Sense users"
    Start-Process powershell.exe -ArgumentList "Start-Process cmd.exe -Verb runAs -ArgumentList '/c C:\provision\updatedir.bat'"
    Restart-Service QlikSenseRepositoryService -Force

} else {
    Write-Error "Error: Qlik Sense Certs not recreated!"
    throw "Error: Qlik Sense Certs not recreated!"
}


#### Recreate QS desktop shortcuts
Write-Log "Recreate QS desktop shortcuts"
$sourcepath="C:\Users\Public\Desktop\Qlik Management Console.lnk"
$destination="C:\Users\Public\Desktop\Qlik Management Console2.lnk"
Copy-Item $sourcepath $destination  ## Get the lnk we want to use as a template
Remove-Item -Path $sourcepath -Force
$shell = New-Object -COM WScript.Shell
$shortcut = $shell.CreateShortcut($destination)  ## Open the lnk
$shortcut.TargetPath = "https://$env:computername/qmc"  ## Make changes
$shortcut.Save()  ## Save
Rename-Item -Path $destination -NewName "Qlik Management Console.lnk"

$sourcepath="C:\Users\Public\Desktop\Qlik Sense Hub.lnk"
$destination="C:\Users\Public\Desktop\Qlik Sense Hub2.lnk"
Copy-Item $sourcepath $destination  ## Get the lnk we want to use as a template
Remove-Item -Path $sourcepath -Force
$shell = New-Object -COM WScript.Shell
$shortcut = $shell.CreateShortcut($destination)  ## Open the lnk
$shortcut.TargetPath = "https://$env:computername/hub"  ## Make changes
$shortcut.Save()  ## Save
Rename-Item -Path $destination -NewName "Qlik Sense Hub.lnk"
####


Write-Log "Resize Partition C to max size"
$size = Get-PartitionSupportedSize -DriveLetter C
Resize-Partition -DriveLetter C -Size $size.SizeMax | Out-Null


REG ADD HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System /v ConsentPromptBehaviorAdmin /t REG_DWORD /d 5 /f




