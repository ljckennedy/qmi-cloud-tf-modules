<#
Module:             qs-post-cfg
Author:             Clint Carr
Modified by:        -
Modification History:
- Fixed connect as user logic (qlik-cli change)
- Added logic to accomodate for Professional/Analyzer or User licenses 
- Added a loop into the Connect-Qlik statement to remove an abort error
- Added Logging
- Added comments
- Error checking
- Modified service connection for Qlik Sense from endless loop to a set number of attempts.
- Added a service restart at the end of the Central Node (seems to resolve an issue with April 2018)
last updated:       10/18/2019
Intent: Configure the Qlik Sense environment with applications and Security Rules.
#>

Param(
    [string] $Hostname,
    [string] $Certpassword,
    [string] $QlikUserPassword

)

Import-Module Qlik-Cli
Import-Module "Carbon"

Install-User -UserName Qlik -Password $QlikUserPassword

Write-Log -Message "Starting qs-post-cfg.ps1"

### Waiting for Qlik Sense installation to complete
#-----------
Function restartQse
{
    Write-Log "Checking Engine Service has started..."
    $qse = get-service QlikSenseEngineService
    write-log -Message "The engine is currently $($qse.Status)"
    if ($qse.Status -eq "Stopped") {
        Write-Log -Message "Starting Qlik Sense Engine and waiting 120 seconds" -Severity "Warn"; 
        Start-Service QlikSenseEngineService ; 
        Restart-Service QlikSenseServiceDispatcher; 
        start-sleep -s 120
    }
    write-log -Message "The engine is currently $($qse.Status)"
}

Function connQSR
{
    $i = 1
    $statusCode = 0
    while ($statusCode -ne 200 -and $i -le 10) 
        { 
            try {$statusCode = (Invoke-WebRequest https://$($env:COMPUTERNAME)/qps/user -UseBasicParsing).statusCode }
            catch
                {
                    $i++
                    write-log -Message "QSR on $env:COMPUTERNAME not responding attempt $i of 10..." -Severity "Warn"
                    start-sleep -s 20 
                }
        } 
}

Function restartServices
{
    write-log -Message "Restarting Qlik Sense Services on $env:COMPUTERNAME" -Severity "Warn"
    Restart-Service QlikSenseRepositoryDatabase -Force
    Restart-Service QlikLoggingService -Force
    Restart-Service QlikSenseServiceDispatcher -Force
    Restart-Service QlikSenseRepositoryService -Force
    Restart-Service QlikSenseProxyService -Force
    Restart-Service QlikSenseEngineService -Force
    Restart-Service QlikSensePrintingService -Force
    Restart-Service QlikSenseSchedulerService -Force
}

Function qlikSenseUserAccess
{
    $userAccessGroup = (@{name = "License Everyone";} | ConvertTo-Json -Compress -Depth 10)
    $licenseId = Invoke-QlikPost "/qrs/License/UserAccessGroup" $userAccessGroup
    $systemRuleJson = (@{
        name = "Grant Everyone a token";
        category = "License";
        rule = '((user.name like "*"))';
        type = "Custom";
        resourceFilter = "License.UserAccessGroup_" + $licenseId.id;
        actions = 1;
        ruleContext = "QlikSenseOnly";
        disabled = $false;
        comment = "Rule to set up automatic user access";} | ConvertTo-Json -Compress -Depth 10)
    Write-Log -Message "Adding user license rule to grant Everyone Tokens."
    try {
        Invoke-QlikPost "/qrs/SystemRule" $systemRuleJson | Out-Null
    } catch {
        Write-Log -Message $_.Exception.Message -Severity "Error"
    }
}

Function qlikSenseProfessionalAccess
{
    $professionalAccessGroup = (@{name = "License Everyone";} | ConvertTo-Json -Compress -Depth 10)
    $licenseId = Invoke-QlikPost "/qrs/License/ProfessionalAccessGroup" $professionalAccessGroup
    $systemRuleJson = (@{
        name = "Grant Everyone Professional Access";
        category = "License";
        rule = '((user.name like "*"))';
        type = "Custom";
        resourceFilter = "License.ProfessionalAccessGroup_" + $licenseId.id;
        actions = 1;
        ruleContext = "QlikSenseOnly";
        disabled = $false;
        comment = "Rule to set up automatic user access";} | ConvertTo-Json -Compress -Depth 10)
    Write-Log -Message "Adding user license rule to grant Everyone access as Professional."
    try {
        Invoke-QlikPost "/qrs/SystemRule" $systemRuleJson | Out-Null
    } catch {
        Write-Log -Message $_.Exception.Message -Severity "Error"
    }
}

#-----------
write-log -Message "Waiting 1 minute for Qlik Sense installation to complete"
start-sleep -s 60

    
### wait for Qlik Sense Proxy service to respond with an HTTP 200 status before proceeding
connQSR
$statusCode = (Invoke-WebRequest https://$($env:COMPUTERNAME)/qps/user -UseBasicParsing).statusCode
if ($statusCode -ne 200)
    { 
        Write-Log -Message "Waiting 25 seconds before next pass" -Severity "Warn"
        restartServices
        Write-Log -Message "Waiting 45 seconds for Services to ensure they are ready" -Severity "Warn"
        start-sleep -s 45
        connQSR
    }

$statusCode = (Invoke-WebRequest https://$($env:COMPUTERNAME)/qps/user -UseBasicParsing).statusCode
if ($statusCode -ne 200) { 
        Write-Log -Message "Provisioning failed" -Severity "Error"
        Exit 
}
Write-Log -Message "Qlik Sense Proxy responding on $env:COMPUTERNAME, status code: $statusCode"
Write-Log -Message "Connecting to Qlik Sense Repository Service on $env:COMPUTERNAME"

restartQse

### Connect to the Qlik Sense Repository Service with Qlik-Cli
do {write-log -Message "Connecting to Qlik Sense Repository"; start-sleep 15} 
While( (Connect-Qlik $($env:COMPUTERNAME) -TrustAllCerts -UseDefaultCredentials -ErrorAction SilentlyContinue).length -eq 0 )


### Enabling HTTP
Write-Log -Message "Enabling HTTP access on Central Node proxy"
try {
    Get-QlikProxy -filter "serverNodeConfiguration.Name eq 'Central'" | Update-QlikProxy -AllowHttp 1  | Out-Null
} catch {
    Write-Log -Message $_.Exception.Message -Severity "Error"
}
Start-Sleep -s 10
### Install qlik-poc_com certificate
Write-Log -Message "Install qlik-poc_com certificate"
try {
    $secpasswd = ConvertTo-SecureString $Certpassword -AsPlainText -Force
    $sslCert=Import-PfxCertificate -FilePath C:/provision/wildcard_qmi_qlik-poc_com.pfx -CertStoreLocation Cert:\LocalMachine\My -Password $secpasswd
    Update-QlikProxy -SslBrowserCertificateThumbprint $sslCert.Thumbprint -id (Get-QlikProxy).id | Out-Null
    Start-Sleep -s 10
    Update-QlikVirtualProxy -id (Get-QlikVirtualProxy -filter "description eq 'Central Proxy (Default)'").id -websocketCrossOriginWhiteList $Hostname | Out-Null
    Start-Sleep -s 10
    Update-QlikVirtualProxy -id (Get-QlikVirtualProxy -filter "description eq 'qib'").id -websocketCrossOriginWhiteList $Hostname | Out-Null
} catch {
    Write-Log -Message $_.Exception.Message -Severity "Error"
}

