
variable "prefix" {
  description = "The Prefix used for all resources in this example"
  default = "QMI-CENTOS"
}

variable "location" {
  default = "East US"
}

variable "resource_group_name" {
}

variable "vm_type" {
  default = "Standard_DS3_v2"
}

variable "admin_username" {
  default = "qmi"
}

variable "user_id" {
}